from backend.storage.interfax.interfax import InterfaxNewsDAO
from backend.wiring import Wiring
from backend.parse.interfax.interfax_parse_impl import interfax_news_server_parse
from backend.parse.moex.moex_parse_impl import moex_news_server_parse
from backend.tasks.task import task

from typing import Dict, List

import pickle
import json

import requests

from tqdm import tqdm

import datetime
from pytz import timezone
import pandas as pd

from finam import Exporter, Market
from finam.exception import FinamObjectNotFoundError

from backend import dev_settings

def get_news(wiring: Wiring, date: pd.Timestamp) -> None:
        if not wiring.global_flags["smartlab"]:
            wiring.logger.info("Start smartlab parse")
            wiring.global_flags["smartlab"] = True
            start = wiring.mongo_client.get_max(collection="news_test", col="date")
            if len(start):
                start = start[0]["date"]
            else:
                start = str((curr_moscow_datetime - pd.Timedelta(days=365*1.5)).date())

            wiring.logger.info(f"Get max value from mongodb: {start}")
            start = pd.to_datetime(start)
            for d in tqdm(pd.date_range(start=str((pd.to_datetime(start + pd.Timedelta(days=1)).date())), end=date), desc="smartlab_news_parse"):
                wiring.logger.info(f"Start smartlab news parsing for date: {d}")
                converted_date = str(d).replace("-", "/")
                news = wiring.smartlab_news_crawler.parse_date_news(converted_date, local=True)
                for elem in news:
                    doc_dict = elem.__dict__.copy()
                    doc_dict.pop("id")
                    for key in doc_dict.keys():
                        doc_dict[key] = "no" if doc_dict[key] is None else doc_dict[key]
                        if key == "date":
                            doc_dict[key] = pd.to_datetime(doc_dict[key])
                    #wiring.mongo_client.replace_one(collection="news_test", filter_exp={"date": doc_dict["date"], "company": doc_dict["company"]}, update_clause=doc_dict, upsert=True)
                    wiring.mongo_client.insert_one("news_test", doc_dict)
                    wiring.logger.info(f"Append to mongodb: {str(elem)}")

def get_moex_quotes(wiring: Wiring, curr_moscow_datetime: pd.Timestamp) -> None:
        if not wiring.global_flags["quotes"]:
            wiring.logger.info("Start quotes moex parse")
            quotes_db = "quotes_test"
            wiring.global_flags["quotes"] = True

            info = wiring.mongo_client.find_many(
                collection="companies", 
                filter_exp=({"exchange": "moex"}),
                is_id=False
            )
            moex_tickers = [t["ticker"] for t in info]

            actual_moex_tickers = []
            #exporter = Exporter()
            for t in moex_tickers:
                    wiring.logger.info(f"Check ticker: {t} on moex stock exchange")
            #    try:
            #        rub = exporter.lookup(code=t, market=Market.SHARES, timeout)
            #    except FinamObjectNotFoundError:
            #        wiring.logger.info(f"Ticker: {t} have not found on moex stock exchange")
            #    else:
            #        wiring.logger.info(f"Ticker: {t} have found on moex stock exchange")
                    actual_moex_tickers.append(t)
            
            wiring.logger.info(f"Length of base tickers: {len(moex_tickers)}, length of actual tickers: {len(actual_moex_tickers)}")
            _max = wiring.mongo_client.find_min_max_value_in_company(collection="quotes_test", 
                filter_exp=(
                    {
                        "company": {"$in": actual_moex_tickers},
                        "date": {"$gt": pd.to_datetime(curr_moscow_datetime) - pd.Timedelta(days=14)}
                    }
                )
            )
            if _max is not None:
                start = str(_max.date())
            else:
                start = str((pd.to_datetime("now") - pd.Timedelta(days=365*1.5)).date())

            quotes = wiring.exchange_crawler.daily_quotes_server_parse(tickers=actual_moex_tickers, start=start)
            wiring.logger.info(f"Write quotes data to db for date, start: {start}")
            for quote in quotes.to_dict("records"):
                #wiring.mongo_client.replace_one(quotes_db, filter_exp={"date": quote["date"], "company": quote["company"]}, update_clause=quote, upsert=True)
                wiring.mongo_client.insert_one(quotes_db, quote)
            wiring.logger.info(f"Writed {len(quotes)} samples")
            
            try:
                wiring.logger.info(f"Send post request to '{wiring.ml_url}' for moex quotes")
                req = requests.post(wiring.ml_url, json={"flag": True, "min_ts": start, "exchange": "moex"}, timeout=5)
            except:
                wiring.logger.info(f"Error in request sending, status: {req.text}")
            else:
                wiring.logger.info(f"Request status: {req.text}")

def get_usa_quotes(wiring: Wiring, curr_moscow_datetime: pd.Timestamp) -> None:
        if not wiring.global_flags["quotes_usa"]:
            wiring.logger.info("Start quotes usa parse")
            quotes_db = "quotes_test"
            wiring.global_flags["quotes_usa"] = True

            info = wiring.mongo_client.find_many(
                collection="companies", 
                filter_exp=({"exchange": "nyse"}),
                is_id=False
            )
            usa_tickers = [t["ticker"] for t in info]
            _max = wiring.mongo_client.find_min_max_value_in_company(collection="quotes_test", filter_exp=({"company": {"$in": usa_tickers}}))
            if _max is not None:
                start = str(_max.date())
            else:
                start = str((pd.to_datetime("now") - pd.Timedelta(days=365*1.5)).date())

            for t in tqdm(usa_tickers, desc="usa tickers iter"):
                quotes_daily, quotes_hourly = wiring.exchange_crawler.get_yf_daily_history(tickers=[t])
                wiring.logger.info(f"Write quotes data to db for date, start: {start}")
                for quote in quotes_daily.to_dict("records"):
                    #wiring.mongo_client.replace_one(quotes_db, filter_exp={"date": quote["date"], "company": quote["company"]}, update_clause=quote, upsert=True)
                    wiring.mongo_client.insert_one(quotes_db, quote)
                wiring.logger.info(f"Writed {len(quotes_daily)} samples")

            try:
                wiring.logger.info(f"Send post request to '{wiring.ml_url}' for usa quotes")
                req = requests.post(wiring.ml_url, json={"flag": True, "min_ts": start, "exchange": "nyse"}, timeout=5)
            except:
                wiring.logger.info(f"Error in request sending, status: {req.text}")
            else:
                wiring.logger.info(f"Request status: {req.text}")

def check_time(wiring: Wiring) -> None:
    wiring.logger.info("Start check_time process")
    curr_moscow_datetime = datetime.datetime.now(timezone('Europe/Moscow'))
    date = str(pd.to_datetime(curr_moscow_datetime.date()))
    wiring.logger.info(f"Curr moscow datetime: {curr_moscow_datetime}")

    if curr_moscow_datetime.hour == wiring.parsing_start_hour and pd.to_datetime(date).weekday() not in (8,9): #(5,6):
         #get_news(wiring, date)
         #get_moex_quotes(wiring, date)
         get_usa_quotes(wiring, date)

    elif curr_moscow_datetime.hour == wiring.reset_global_flags_hour:
        wiring.logger.info("Reset wiring global_flags")
        for source in list(wiring.global_flags.keys()):
            wiring.global_flags[source] = False
    
    else:
        if pd.to_datetime(date).weekday() in (5,6):
            wiring.logger.info("There is weekday today, exchanges does not work")
        
        if curr_moscow_datetime.hour != wiring.parsing_start_hour:
            wiring.logger.info(f"Current moscow datetime hour == {curr_moscow_datetime.hour} does not equal parsing_start_hour == {wiring.parsing_start_hour}")
        
        if curr_moscow_datetime.hour != wiring.reset_global_flags_hour:
            wiring.logger.info(f"Current moscow datetime hour == {curr_moscow_datetime.hour} does not equal reset_global_flags_hour == {wiring.reset_global_flags_hour}")
