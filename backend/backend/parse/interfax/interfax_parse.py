from typing import List
from backend.storage.interfax.interfax import InterfaxNews
from backend.wiring import Wiring

# парсинг Интерфакса

class InterfaxParser(object):

    def get_single_news(self, url: str) -> InterfaxNews:
        pass

    def get_day_news_ids(self, url: str) -> List[str]:
        pass

    def parse_day_news(self, date: str, local: bool = False) -> List[InterfaxNews]:
        pass

class ServerInterfaxParser(object):

    def parse_day_news(self, date: str) -> None:
        pass