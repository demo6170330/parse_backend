from bs4 import BeautifulSoup
import requests

from multiprocessing import Pool
import multiprocessing

from calendar import Calendar
from typing import List

from backend.storage.interfax.interfax import InterfaxNews, InterfaxNewsDAO
#from backend.backend.tasks.task import task
from backend.other import find_between


class InterfaxParser(InterfaxNews):

    def __init__(self):
        self.base_link: str = 'https://www.interfax.ru/'

    @classmethod
    def get_single_news(cls, url: str) -> InterfaxNews:
        link = requests.get(url)
        link.encoding = None
        b = BeautifulSoup(link.text, "html.parser")
        base = b.body.find(
            'div', {'class': 'infinitblock'}
        )
        if base is None:
            return None
        header = find_between(
            base.find(
                'h1', {'itemprop': 'headline'}
            ), start='">', end="</h1>"
        ).group(1)
        date = find_between(
            base.find(
                'aside', {'class': 'textML'}
            ).find_all(
                'time'
            )[0],
            start='datetime="', end='">'
        ).group(1)
        article = ' /n '.join(
            [
                v.text for v in b.body.find(
                    'article', {'itemprop': 'articleBody'}
                ).find_all('p')
            ]
        ).replace('<p>', '').replace('</p>', '')
        try:
            body = b.body.find(
                'div', {'class', 'textMTags'}
            )
        except AttributeError:
            tags: str = ''
        else:
            if body:
                tags: List[str] = [
                    find_between(s, start='">', end='</a>').group(1) for s in
                    body.find_all(
                        'a'
                    )
                ]
                for i, tag in enumerate(tags):
                    if len(tag.split()) > 1:
                        tags[i] = '_'.join(tag.split())
                tags: str = ' '.join(tags)
            else:
                tags: str = ''

        return InterfaxNews(
        **{
            'date': date, 'header': header,
            'article': article, 'tags': tags,
            'link': url
        }, format='', only_date=False
    )

    @classmethod
    def get_day_news_ids(cls, url: str) -> List[str]:
        link = requests.get(url)
        link.encoding = None
        b = BeautifulSoup(link.text, "html.parser")
        ids = [
            find_between(s, start='/', end='">').group(1) for s in b.body.find(
                'div', {'class': 'an'}
            ).find_all(
                'a'
            )
        ]
        return ids

    def parse_day_news(self, date: str, local: bool = False) -> List[InterfaxNews]: #date example: 2020-06-15
        date = date.replace("-", "/")
        links: List[str] = [
            self.base_link + value
            for value in self.get_day_news_ids(f'{self.base_link}/business/news/{date}')
        ]

        if not local:
            manager = multiprocessing.Manager()
            shared_list = manager.list()
            with Pool(15) as p:
                shared_list.append(p.map(self.get_single_news, links))

            news = [v for v in shared_list[0] if v is not None]
        else:
            news = list(map(self.get_single_news, links))
        return news

def get_single_news(url: str) -> InterfaxNews:
    link = requests.get(url)
    link.encoding = None
    b = BeautifulSoup(link.text, "html.parser")
    news = None
    base = b.body.find(
        'div', {'class': 'infinitblock'}
    )
    header = find_between(
        base.find(
            'h1', {'itemprop': 'headline'}
        ), start='">', end="</h1>"
    ).group(1)
    date = find_between(
        base.find(
            'aside', {'class': 'textML'}
        ).find_all(
            'time'
        )[0],
        start='datetime="', end='">'
    ).group(1)
    article = ' /n '.join(
        [
            v.text for v in b.body.find(
                'article', {'itemprop': 'articleBody'}
            ).find_all('p')
        ]
    ).replace('<p>', '').replace('</p>', '')
    try:
        body = b.body.find(
            'div', {'class', 'textMTags'}
        )
    except AttributeError:
        tags: str = ''
    else:
        if body:
            tags: List[str] = [
                find_between(s, start='">', end='</a>').group(1) for s in
                body.find_all(
                    'a'
                )
            ]
            for i, tag in enumerate(tags):
                if len(tag.split()) > 1:
                    tags[i] = '_'.join(tag.split())
            tags: str = ' '.join(tags)
        else:
            tags: str = ''

    return InterfaxNews(
        **{
            'date': date, 'header': header,
            'article': article, 'tags': tags,
            'link': url
        }, format='', only_date=False
    )

def get_day_news_ids(cls, url: str) -> List[str]:
    link = requests.get(url)
    link.encoding = None
    b = BeautifulSoup(link.text, "html.parser")
    ids = [
        find_between(s, start='/', end='">').group(1) for s in b.body.find(
            'div', {'class': 'an'}
        ).find_all(
            'a'
        )
    ]
    return ids

#@task
def interfax_news_server_parse(interfax_news_dao: InterfaxNewsDAO, date: str) -> None:
    date = date.replace("-", "/")
    base_link = 'https://www.interfax.ru/'
    links = [
        base_link + value
        for value in get_day_news_ids(f'{base_link}/business/news/{date}')
    ]
    news = list(map(get_single_news, links))

    for single in news:
        interfax_news_dao.create(single)

if __name__ == '__main__':
    manager = multiprocessing.Manager()
    c = Calendar()

    parser = InterfaxParser()
    #for s in parser.parse_day_news('2021-01-15'):
    #    print(s)

    from tqdm import tqdm
    import csv
    import copy
    for year in tqdm(range(2008, 2022), desc='year'):
        for month in tqdm(range(1, 13), desc='month'):
            news = []
            for day in tqdm(c.itermonthdates(year, month), desc='parsing'):
                news.extend(
                    parser.parse_day_news(str(day).replace('-', '/'))
                )

            with open('interfax_company_news.csv', 'a') as outfile:
                w = csv.DictWriter(outfile, fieldnames=['id', 'company', 'date', 'header', 'article', 'link', 'sentiment', 'tags', 'tickers'])
                w.writeheader()
                for row in copy.deepcopy(news):
                    w.writerow(row.__dict__)
