import requests
from bs4 import BeautifulSoup

from multiprocessing import Pool
import multiprocessing

from calendar import Calendar

import pickle

from backend.other import find_between, convert_date, Datetime
from backend.storage.interfax.interfax import InterfaxNews, InterfaxNewsDAO

#from backend.backend.storage.connector import renew_connection, get_tor_session

from typing import Tuple, List, Dict
from tqdm import tqdm

is_tor = False

class SmartLabNewsParser(InterfaxNews):

    def __init__(self):
        self.news_link = "https://smart-lab.ru/news"
        self.blog_link = "https://smart-lab.ru/blog/news"

    def get_date_news_links(self, date: str) -> Tuple[List[str], bool]: #example. date: 05.11.2020
        dt = Datetime(date).date
        url = f'{self.news_link}/date/{dt}/'
        link = None

        if is_tor:
            renew_connection()
            s = get_tor_session()
            link = s.get(url)
        else:
            link = requests.get(url)

        link.encoding = 'UTF-8'
        b = BeautifulSoup(link.text, "html.parser")

        news = b.body.find(
            'div', attrs={'class': 'topic allbloglist'}
        )
        if news:
            links = news.find_all(
                'a'
            )
            for_text = [
                    find_between(string, start='/blog/', end='" title') 
                    for string in links
            ]
            for i in range(len(for_text)):
                try:
                    var = for_text.group(1)
                except:
                    pass
                else:
                    for_text[i] = var
            return (for_text, True)
        return (None, False)

    def _get_single_news(self, link: str) -> InterfaxNews:
        url = f'{self.blog_link}/{link}'
        link = None

        try:
            if is_tor:
                s = get_tor_session()
                link = s.get(url)
            else:
                link = requests.get(url)
        except requests.exceptions.ConnectionError:
            return None

        link.encoding = 'UTF-8'
        b = BeautifulSoup(link.text, "html.parser")

        if b.body is None:
            return InterfaxNews(**{"link": url})

        b = b.find("div", {"id": "content"})
 
        header_title_h1 = b.body.find("h1", {"class": "title"})
        header = ""
        if header_title_h1 is not None:
            header = find_between(
                header_title_h1.find('span'),
                start='<span>', end='</span>'
            ).group(1)

        article = b.body.find(
            'div', {'id': 'content'}
        ).find(
            'div', {'class': 'content'}
        ).text

        time = b.body.find(
            'li', {'class': 'date'}
        ).text[-5:]

        tags: List[str] = [
            find_between(s, start='">', end='</a>').group(1)
            for s in
                b.body.find(
                    'div', {'id': 'content'}
                ).find(
                    'ul', {'class', 'tags'}
                ).find_all(
                    'a'
                )
        ]

        for i, tag in enumerate(tags):
            if len(tag.split()) > 1:
                tags[i] = '_'.join(tag.split())
        tags: str = ' '.join(tags)

        t_body = b.body.find(
                'div', {'id': 'content'}
            ).find(
                'ul', {'class', 'forum_tags'}
            )

        tickers = None
        if t_body is not None:
            tickers: List[str] = []
            for s in t_body.find_all('a'):
                cond = find_between(s, start='"/forum/', end='" title')
                if cond:
                    tickers.append(cond.group(1))

            for i, ticker in enumerate(tickers):
                if len(ticker.split()) > 1:
                    tickers[i] = '_'.join(ticker.split())
            tickers: str = ' '.join(tickers)

        return InterfaxNews(**{'header': header, 'article': article, 'link': url, 'tags': tags, 'tickers': tickers, 'sentiment': time})

    def get_single_news(self, links: List[str]) -> List[InterfaxNews]:
        news = [self._get_single_news(link) for link in links]
        return news

    def parse_date_news(self, date: str, local: bool = False) -> List[InterfaxNews]:
        dt = Datetime(date).date
        links, flag = self.get_date_news_links(date=dt)
        if not flag:
            return []

        if not local:
            news = multiprocessing.Manager().list()
            with Pool(5) as p:
                news.extend(p.map(
                    self._get_single_news, links
                ))
            if len(news):
                news = [v for v in news if v is not None]
        else:
            news: List[InterfaxNews] = self.get_single_news(links)

        updated_news: List[InterfaxNews] = []
        for elem in news:
            elem.date = Datetime(dt + " " + elem.sentiment, only_date=False).date
            elem.sentiment = -1
            if elem.tickers and elem.tickers != '':
                for ticker in elem.tickers.split(" "):
                    elem.company = ticker
            updated_news.append(elem)
        return updated_news

    def parse_year_news(self, year:int) -> List[InterfaxNews]:
        c = Calendar()
        all_news = []
        for month in tqdm((range(1, 13)), desc=f"{year} news parsing"):
            news = []
            for day in c.itermonthdates(year, month):
                news.extend(
                    self.parse_date_news(
                        date=str(day).replace('-', '.')
                    )
                )
            all_news.extend(news)
        return all_news

    def parse_all_years_news(self, start_year=2010, end_year=2021) -> List[InterfaxNews]:
        news = []
        for year in range(start_year, end_year + 1):
            n = self.parse_year_news(year)

            import csv

            with open('smartlab_news.csv', 'a') as outfile:
                w = csv.DictWriter(outfile,
                                   fieldnames=['id', 'company', 'date', 'header', 'article', 'link', 'sentiment',
                                               'tags', 'tickers'])
                w.writeheader()
                for row in n:
                    if row.__dict__["date"] != '':
                        w.writerow(row.__dict__)

            news.extend(n)
        return news

class SmartLabCommentsParser(InterfaxNews):

    def __init__(self):
        with open('companies.json', 'rb') as f:
            self.moex_names: List[str] = pickle.load(f)
        self.base_link = 'https://smart-lab.ru/forum'

    def get_date_comments(self, company: str, date: str) -> Tuple[List[str], bool]: #example. date: 05.11.2020
        dt = Datetime(date).date
        url = f'{self.base_link}/{company}/{dt}'
        link = requests.get(url)

        if link.status_code == 404:
            return (None, False)

        link.encoding = 'UTF-8'
        b = BeautifulSoup(link.text, "html.parser")

        pg = b.body.find(
            'span', {'class', 'page active'}
        )
        flag_find=None
        if not pg:
            if b.body.find(
                    'span', {'class', 'page active clock'}
            ):
                flag_find = False
            else:
                return (None, False)

        news = []
        if flag_find:
            max_page = int(
                find_between(pg, start='active">', end='</').group(1)
            )
            for page in range(max_page + 1):
                news.extend(self._parse_page_comments(
                    company=company,
                    date=dt,
                    page=page
                ))
        else:
            news.extend(self._parse_page_comments(
                company=company,
                date=dt,
                page=0
            ))
        return (news, True)

    def _parse_page_comments(self, company: str, date: str, page: int):
        dt = Datetime(date).date
        if page != 0:
            url = f'{self.base_link}/{company}/{dt}/page{page}/'
        else:
            url = f'{self.base_link}/{company}/{dt}/'
        link = requests.get(url)

        link.encoding = 'UTF-8'
        b = BeautifulSoup(link.text, "html.parser")

        comments = [
                InterfaxNews(
                    **{
                        'date': date,
                        'article': elem.text,
                        'link': url,
                        'company': company
                    }
                ) for elem in b.body.find_all(
                'div', {'class', 'text'}
            )
        ]
        return comments

    def parse_all_companies_comments(self, date: str, local: bool = False) -> List[InterfaxNews]:
        comments = []
        for c in tqdm(self.moex_names, desc='parsing'):
            c_day_news, flag = self.get_date_comments(
                company=c,
                date=date
            )
            if flag:
                comments.extend(c_day_news)
        return comments

if __name__ == "__main__":
    #p = SmartLabCommentsParser()
    #coms, flag = p.get_date_comments(company="SBER", date="2020-05-12")

    p = SmartLabNewsParser()
    p.parse_all_years_news(start_year=2017, end_year=2021)
