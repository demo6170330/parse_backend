import requests
from bs4 import BeautifulSoup
import os

import pandas as pd

from multiprocessing import Pool
import multiprocessing

import csv
import copy

import pickle
from backend.other import find_between, convert_date, Datetime
from backend.storage.interfax.interfax import InterfaxNews, InterfaxNewsDAO
from backend.storage.smartlab.smartlab_impl import MongoSmartLabNewsDAO, MongoSmartLabCommentsDAO
from backend.wiring import Wiring
from backend.tasks.task import task

from typing import Tuple, List, Dict
from tqdm import tqdm

def get_date_news_links(date: str) -> Tuple[List[str], bool]: #example. date: 05.11.2020
    news_link = "https://smart-lab.ru/news"
    dt = Datetime(date).date
    url = f'{news_link}/date/{dt}/'
    link = None

    link = requests.get(url)

    link.encoding = 'UTF-8'
    b = BeautifulSoup(link.text, "html.parser")

    news = b.body.find(
        'div', attrs={'class': 'topic allbloglist'}
    )
    if news:
        links = news.find_all(
            'a'
        )
        for_text = [
            find_between(string, start='/blog/', end='" title').
                group(1) for string in links
        ]
        return (for_text, True)
    return (None, False)

def _get_single_news(link: str) -> InterfaxNews:
    blog_link = "https://smart-lab.ru/blog/news"
    url = f'{blog_link}/{link}'
    link = None

    link = requests.get(url)
    link.encoding = 'UTF-8'
    b = BeautifulSoup(link.text, "html.parser")

    header = find_between(
        b.body.find(
            'h1', {'class': 'title'}
        ).find(
            'span'
        ),
        start='<span>', end='</span>'
    ).group(1)

    article = b.body.find(
        'div', {'id': 'content'}
    ).find(
        'div', {'class': 'content'}
    ).text

    time = b.body.find(
        'li', {'class': 'date'}
    ).text[-5:]

    tags: List[str] = [
        find_between(s, start='">', end='</a>').group(1)
        for s in
            b.body.find(
                'div', {'id': 'content'}
            ).find(
                 'ul', {'class', 'tags'}
            ).find_all(
                'a'
            )
    ]

    for i, tag in enumerate(tags):
        if len(tag.split()) > 1:
            tags[i] = '_'.join(tag.split())
    tags: str = ' '.join(tags)

    t_body = b.body.find(
            'div', {'id': 'content'}
        ).find(
            'ul', {'class', 'forum_tags'}
        )

    tickers = None
    if t_body is not None:
        tickers: List[str] = []
        for s in t_body.find_all('a'):
            cond = find_between(s, start='"/forum/', end='" title')
            if cond:
                tickers.append(cond.group(1))

        for i, ticker in enumerate(tickers):
            if len(ticker.split()) > 1:
                tickers[i] = '_'.join(ticker.split())
        tickers: str = ' '.join(tickers)

    return InterfaxNews(**{'header': header, 'article': article, 'link': url, 'tags': tags, 'tickers': tickers, 'sentiment': time})

def get_single_news(links: List[str]) -> List[InterfaxNews]:
    news = [_get_single_news(link) for link in links]
    return news

def get_date_comments(company: str, date: str) -> Tuple[List[InterfaxNews], bool]: #example. date: 05.11.2020
    dt = Datetime(date).date
    base_link = 'https://smart-lab.ru/forum'
    url = f'{base_link}/{company}/{dt}'
    link = requests.get(url)
    flag_find = True

    if link.status_code == 404:
        return (None, False)

    link.encoding = 'UTF-8'
    b = BeautifulSoup(link.text, "html.parser")

    pg = b.body.find(
        'span', {'class', 'page active'}
    )
    if not pg:
        if b.body.find(
                'span', {'class', 'page active clock'}
        ):
            flag_find = False
        else:
            return (None, False)

    news = []
    if flag_find:
        max_page = int(
            find_between(pg, start='active">', end='</').group(1)
        )
        for page in range(max_page + 1):
            news.extend(_parse_page_comments(
                company=company,
                date=dt,
                page=page
            ))
    else:
        news.extend(_parse_page_comments(
            company=company,
            date=dt,
            page=0
        ))
    return (news, True)

def _parse_page_comments(company: str, date: str, page: int) -> List[InterfaxNews]:
    dt = Datetime(date).date
    base_link = 'https://smart-lab.ru/forum'
    if page != 0:
        url = f'{base_link}/{company}/{dt}/page{page}/'
    else:
        url = f'{base_link}/{company}/{dt}/'
    link = requests.get(url)

    link.encoding = 'UTF-8'
    b = BeautifulSoup(link.text, "html.parser")

    comments = [
            InterfaxNews(
                **{
                    'date': date,
                    'article': elem.text,
                    'link': url,
                    'company': company
                }
            ) for elem in b.body.find_all(
            'div', {'class', 'text'}
        )
    ]
    return comments

def get_smartlab_moex_tickers() -> List[str]:
    base_link = 'https://smart-lab.ru/q/shares/'
    link = requests.get(base_link)

    link.encoding = 'UTF-8'
    b = BeautifulSoup(link.text, "html.parser")

    tickers = []
    for link in tqdm(
            b.body.find(
                'table', {'class': 'simple-little-table trades-table'}
            ).find_all('tr'),
        desc='tickers_parse'
    ):
        found = find_between(link, start='/forum/', end='"')
        try:
            found = found.group(1)
        except AttributeError:
            pass
        else:
            tickers.append(found)
    return tickers

#@task
def smartlab_news_server_parse(date: str) -> List[InterfaxNews]:
    dt = Datetime(date).date
    links, flag = get_date_news_links(date=dt)
    if not flag:
        return False
        
    news: List[InterfaxNews] = get_single_news(links)

    updated_news: List[InterfaxNews] = []
    for elem in news:
        elem.date = Datetime(dt + " " + elem.sentiment, only_date=False)
        elem.sentiment = -1
        if elem.tickers and elem.tickers != '':
            for ticker in elem.tickers.split(" "):
                elem.company = ticker
        updated_news.append(elem)
    return updated_news

#@task
def smartlab_comments_server_parse(smartlab_comments_dao: MongoSmartLabCommentsDAO, date: str, companies: List[str], local: bool = False) -> None:
    comments = []
    for c in tqdm(companies, desc='smartlab_parsing'):
        c_day_comments, flag = get_date_comments(
            company=c,
            date=date
        )
        if flag:
            comments.extend(c_day_comments)

    for el in comments:
        try:
            smartlab_comments_dao.create(el)
        except:
            pass