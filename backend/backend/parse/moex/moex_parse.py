from backend.backend.other import Datetime
from backend.storage.interfax.interfax import InterfaxNews
from backend.wiring import Wiring

from typing import List

# парсинг MOEX

class MOEXParser(object):

    def get_single_news(self, url: str) -> InterfaxNews:
        pass

    def get_day_news_ids(self, url: str) -> List[str]:
        pass

    def parse_day_news(self, date: str, local: bool = False) -> List[InterfaxNews]:
        pass

class ServerMoexParser(object):

    def parse_day_news(self, date: str) -> None:
        pass