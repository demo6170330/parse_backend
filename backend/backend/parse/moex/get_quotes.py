from backend.storage.moex.moex_quotes import QuotesNotFoundError, quotes_to_pd_dataframe, Quotes
from backend.parse.moex.quotes_parse_impl import MOEXQuotesParser
from backend.other import timestamp_to_str

from backend.utils.logger import logger_init

from pytz import timezone
import yfinance as yf

import numpy as np
import pandas as pd

from typing import List

import json

from tqdm import tqdm

import time

class ExchangeCrawler(MOEXQuotesParser):

    def __init__(self):
        super().__init__()

        self.logger = logger_init(logger_name="ExchangeCrawler", logger_path="", logger_level=20, file_logger_need=False,
                max_volume_of_log_file_megabytes=100)

        self.logger.info("logger initialized")

    def get_daily_quotes(self, tickers: List[str], start: str="2015-01-01") -> pd.DataFrame:
        """
        Daily quotes parsing function, start quotes parsing with start date (:param start) and end by current date
        :param tickers: List of strings (tickers)self.logger = l
            format example: ["SBER", "LKOH", "MAIL", "GAZP"]
        :param start: string start date (point from which data collection begins)
            format example: "2015-01-01"
        :return: pd.DataFrame - quotes table
            output columns: columns (id, ts, open, high, low, close, vol, average_open_hourly_price)
        """

        alphas_df = []

        for index, ticker in enumerate(tickers):
            self.logger.info(f"Start quotes parsing for ticker: {ticker}, index: {index}, tickers length: {len(tickers)}")
            try:
                self.logger.info(f"Get daily quotes for ticker: {ticker}, start: {start}")
                q = self.parse_moex_quotes(ticker=ticker, start=start, period="daily")
                quotes_daily = quotes_to_pd_dataframe(q)

            except (ValueError, QuotesNotFoundError) as e:
                self.logger.error(f"quotes_not_found_error: {ticker}")
            else:
                time.sleep(2)
                try:
                    quotes_hourly_list = []
                    flag = False
                    start_date = pd.to_datetime(timestamp_to_str(quotes_daily["date"].min()))
                    self.logger.info(f"Start date for '{ticker}': {str(start_date)}")
                    while not flag:
                        if start_date + pd.Timedelta(days=5 * 365) > pd.to_datetime(
                                timestamp_to_str(quotes_daily["date"].max())):
                            self.logger.info(f"Get hourly quotes for ticker: {ticker}, start: {timestamp_to_str(start_date)}, end: {timestamp_to_str(quotes_daily['date'].max())}")
                            quotes_hourly = quotes_to_pd_dataframe(
                                self.parse_moex_quotes(ticker=ticker, start=timestamp_to_str(start_date), end=timestamp_to_str(quotes_daily["date"].max()),
                                                    period="hour"), cast_cols=True)
                            flag = True
                        else:
                            self.logger.info(f"Get hourly quotes for ticker: {ticker}, start: {timestamp_to_str(start_date)}, end: {timestamp_to_str(start_date + pd.Timedelta(days=5 * 365))}")
                            quotes_hourly = quotes_to_pd_dataframe(
                                self.parse_moex_quotes(ticker=ticker, start=timestamp_to_str(start_date),
                                                    end=timestamp_to_str(start_date + pd.Timedelta(days=5 * 365)),
                                                    period="hour"), cast_cols=True)
                        start_date += pd.Timedelta(days=5 * 365)
                        quotes_hourly_list.append(quotes_hourly)
                    quotes_hourly = pd.concat(quotes_hourly_list, axis=0).reset_index(drop=True)
                except (QuotesNotFoundError, KeyError) as e:
                    pass
                else:
                    try:
                        quotes_daily["average_open_hourly_price"] = quotes_hourly.groupby("date").aggregate("mean")["open"].values
                    except ValueError:
                        pass
                    else:
                        quotes_daily["id"] = [index for i in range(len(
                            quotes_daily))]  # если в датасете несколько компаний, присваивается уникальный ключ для каждой компании, как в этом примере
                        quotes_daily = quotes_daily.rename(columns={"date": "ts"})
                        arrays = [quotes_daily['id'].tolist(), quotes_daily['ts'].tolist()]
                        quotes_daily = quotes_daily.drop('ts', axis=1)
                        index = pd.MultiIndex.from_arrays(arrays, names=('id', 'ts'))
                        quotes_daily = pd.DataFrame(quotes_daily.values, columns=quotes_daily.columns, index=index)
                        quotes_daily = quotes_daily.drop('id', axis=1)
                        for col in ("vol", "close", "low", "high", "open", "average_open_hourly_price"):
                            quotes_daily[col] = quotes_daily[col].astype("float")
                        alphas_df.append(quotes_daily)

                        self.logger.info(f"Finish quotes parsing for ticker: {ticker}")
        
        trades_data = pd.concat(alphas_df, axis=0)
        trades_data = trades_data.drop(["company", "per", "time"], axis=1)

        self.logger.info(f"End quotes parsing method for date start: {start}")
        return trades_data

    def daily_quotes_server_parse(self,
            tickers: List[str], start: str,
        ) -> pd.DataFrame:
        daily_quotes = self.get_daily_quotes(tickers, start).reset_index()
        daily_quotes = daily_quotes.rename(columns={"id": "company", "ts": "date"})
        daily_quotes["company"] = daily_quotes["company"].map(dict(zip([i for i in range(len(tickers))],  tickers)))
        daily_quotes["date"] = pd.to_datetime(daily_quotes["date"])
        return daily_quotes

    def get_yf_daily_history(self, tickers: List[str], end_date=str(pd.to_datetime(str(pd.datetime.now(timezone('US/Eastern')))[:14] + "00:00").date())) -> pd.DataFrame:
        cat_daily, cat_hourly = [], []
        self.logger.info("YF daily history parsing start")
        tiks = yf.Tickers(" ".join(tickers))
        for index, ticker in enumerate(tickers):
            #t = yf.Ticker(ticker, session=session)
            self.logger.info(f"Get data for ticker='{ticker}', interval='1d', period='max', end_date='{end_date}', index: {index}, tickers length: {len(tickers)}")
            hist = tiks.tickers.get(ticker).history(interval="1d", period="max", end_date=end_date)
            #["id","ts","open","high","low","close","vol","average_open_hourly_price"]
            sample = hist.reset_index().dropna().rename(columns={"Date": "ts", "Open": "open", "High": "high", "Low": "low", "Close": "close", "Volume": "vol"})
            sample["id"] = [ticker for i in range(len(sample))]
            sample = sample[["id", "ts", "open", "high", "low", "close", "vol"]]

            time.sleep(2)
            
            sub_hourly = []
            try:
                date_range = pd.date_range(start=str(sample["ts"].max().date() - pd.Timedelta(days=365)),
                                        end=str(sample["ts"].max().date()), freq="365d")
                for _min, _max in zip(date_range[:-1], date_range[1:]):
                    self.logger.info(f"Get data for ticker='{ticker}', interval='1h', period='max', start='{str(_min.date())}', end='{str((_max + pd.Timedelta(days=1)).date())}'")
                    d = tiks.tickers.get(ticker).history(interval="1h", period="max", start=str(_min.date()), end=str((_max + pd.Timedelta(days=1)).date())).reset_index().dropna().rename(columns={"index": "ts", "Open": "open", "High": "high", "Low": "low", "Close": "close", "Volume": "vol"})
                    #d = yf.download(ticker, start=str(_min.date()), end=str((_max + pd.Timedelta(days=1)).date()), interval="1h", group_by="ticker").reset_index().dropna().rename(columns={"index": "ts", "Open": "open", "High": "high", "Low": "low", "Close": "close", "Volume": "vol"})
                    sub_hourly.append(d)

                sub_hourly = pd.concat(sub_hourly, axis=0)
                sub_hourly["id"] = [ticker for i in range(len(sub_hourly))]

                sub_hourly = sub_hourly.drop_duplicates(subset=["id", "ts"]).sort_values(by=["id", "ts"])
                sub_hourly["dt"] = pd.to_datetime(sub_hourly["ts"].dt.date)

                _open = sub_hourly.groupby(["id", "dt"])["open"].mean().reset_index().rename(columns={"dt": "ts", "open": "average_open_hourly_price"})
                sample = pd.merge(sample, _open, on=["id", "ts"])

                time.sleep(2)
            except:
                pass
            else:
                cat_hourly.append(sub_hourly.drop("dt", axis=1)[["id", "ts", "open", "high", "low", "close", "vol"]])
            cat_daily.append(sample)

        self.logger.info("YF daily history downloaded")
        if not len(cat_daily) or not len(cat_hourly):
            return pd.DataFrame(columns=["company", "date", "open", "high", "low", "close", "vol"]), pd.DataFrame(columns=["company", "date", "open", "high", "low", "close", "vol"])
        return pd.concat(cat_daily, axis=0).sort_values(by=["id", "ts"]).rename(columns={"id": "company", "ts": "date"}), pd.concat(cat_hourly, axis=0).sort_values(by=["id", "ts"]).rename(columns={"id": "company", "ts": "date"})