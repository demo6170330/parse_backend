import requests
from bs4 import BeautifulSoup
import os

import pandas as pd

from multiprocessing import Pool
import multiprocessing

import csv
import copy

from tqdm import tqdm

from backend.other import find_between, convert_date, Datetime
from backend.storage.interfax.interfax import InterfaxNews, InterfaxNewsDAO
from backend.storage.moex.moex_impl import MongoMOEXNewsDAO
from backend.tasks.task import task

from urllib.parse import urlencode
from urllib.request import urlopen

from typing import Tuple, List, Dict
from datetime import datetime

import time

class NewsByDateNotFound(Exception):
    pass

class TickerNotFoundError(Exception):
    pass

class QuotesNotFoundForDate(Exception):
    pass

class MOEXParser(InterfaxNews):

    def __init__(self):
        pass

    @classmethod
    def get_single_news(cls, fake_link: str) -> InterfaxNews:
        link = None
        for _ in range(5):
            try:
                link = requests.get('https://www.finam.ru/' + fake_link)
            except:
                time.sleep(0.5)
                continue
            else:
                break

        if not link:
            return InterfaxNews(**{'date': '', 'header': '', 'article': ''})

        link.encoding = None
        b = BeautifulSoup(link.text, "html.parser")
        page = b.body.find(
            'div', attrs={'id': 'news-item'}
        )

        if not page:
            return InterfaxNews(**{'date': '', 'header': '', 'article': ''})

        header = find_between(str(page), start='<h1>', end='</h1>')

        if not header:
            return InterfaxNews(**{'date': '', 'header': '', 'article': ''})

        header = header.group(1)
        date_topic = find_between(
            str(page),
            start='<div class="sm lightgrey mb05 mt15">',
            end='</div>'
        ).group(1)
        date = '/'.join(reversed(str(date_topic[:10]).split('.'))) + " " + date_topic[11:16]
        article = page.find("div", {"class": "handmade mid f-newsitem-text"}).get_text()

        return InterfaxNews(**{
            'date': date,
            'header': header,
            'article': article,
            'link': 'https://www.finam.ru/' + fake_link
        }, format='', only_date=False)

    @classmethod
    def get_moex_company_page_news(
            cls,
            url: str='https://www.finam.ru/profile/moex-akcii/pllc-yandex-n-v/news/',
            page: int=1
    ) -> Tuple[List[str], bool]:
        link = None
        for _ in range(5):
            try:
                link = requests.get(url + f'?page={page}')
            except:
                time.sleep(2)
                continue
            else:
                break

        if not link:
            return (None, False)

        link.encoding = None
        b = BeautifulSoup(link.text, "html.parser")

        if not b.body:
            return (None, False)

        news = b.body.find(
            'tbody', attrs={'class': 'news'}
        )
        if news:
            links = news.find_all(
                'div', {'class': 'subject'}
            )
            for_text = [
                find_between(string, start='<a href="/', end='">').
                    group(1) for string in links
            ]
            return (for_text, True)
        return (None, False)

    @staticmethod
    def match_moex_names(tag: str) -> str:
        url = f'https://www.finam.ru/profile/moex-akcii/{tag}/export/'
        link = requests.get(url)
        link.encoding = None
        b = BeautifulSoup(link.text, "html.parser")
        if not b.body:
            return ''

        name = find_between(
            b.body.find(
                'tr',
                {'id': 'issuer-profile-export-name-row'}
            ),
            start='value="', end='"/><').group(1)
        return name

    def parse_moex_companies(self, tags: Dict[str, str], save_path: str=None, local: bool=False) -> List[InterfaxNews]:
        server_news = []
        if local:
            manager = multiprocessing.Manager()
            shared_list = manager.list()
        else:
            manager, shared_list = None, None
            server_news = []

        for tag in tqdm(tags.keys(), desc="parsing"):
            flag = True
            page_number = 1

            while flag:
                fake_links, flag = self.get_moex_company_page_news(
                    url=f'https://www.finam.ru/profile/moex-akcii/{tag}/news/',
                    page=page_number
                )
                if flag:
                    if local:
                        with Pool(15) as p:
                            shared_list.extend(
                                p.map(
                                    self.get_single_news,
                                    fake_links
                                )
                            )

                        with open(save_path, 'a') as outfile:
                            for row in copy.deepcopy(shared_list):
                                w = csv.DictWriter(outfile,
                                                   fieldnames=['id', 'company', 'date', 'header', 'article', 'link',
                                                               'sentiment', 'tags'])
                                if row.__dict__["date"] != '':
                                    row.company = tags[tag]
                                    w.writerow(row.__dict__)
                            shared_list = manager.list()
                    else:
                        news = list(map(
                                self.get_single_news,
                                fake_links
                            ))
                        for i, row in enumerate(news):
                            if row.__dict__["date"] != '':
                                news[i].company = tags[tag]
                        server_news.extend(news)
                    page_number += 1
        return server_news

def merge_moex_quotes_and_news(
        companies, news_file='parsing/parse_data/moex/news/news.csv',
        quotes_path='parsing/parse_data/moex/quotes/',
        save_path='parsing/parse_data/moex/merged/',
        csv=True
):
    news = pd.read_csv(news_file, names=['id', 'company', 'date', 'header', 'article', 'link',
                                                               'sentiment', 'tags'])
    news["date"] = news["date"].astype(str)
    news["date"] = news["date"].apply(lambda x: x[:len(x) - 5] + "00:00")
    print(news)
    finam_names, moex_names = list(companies.keys()), list(companies.values())
    inversed_companies = dict(zip(moex_names, finam_names))
    for name in os.listdir(quotes_path):
        company_quotes = pd.read_csv(quotes_path + name)
        company_quotes["date"] = company_quotes["date"].astype(str)
        company_quotes["date"] = company_quotes["date"].apply(lambda x: x[:len(x) - 5] + "00:00")
        print(company_quotes)
        sub_news = news[news['company'] == inversed_companies[name[:-4]]]
        if len(sub_news):
            result = pd.merge(company_quotes, sub_news, how='left', on=['date'])
            if csv:
                result.to_csv(f'{save_path}{name}', index=False)
            else:
                result.to_excel(f'{save_path}{name[:-4]}.xlsx', index=False)

def get_single_news(fake_link: str) -> InterfaxNews:
    link = None
    for _ in range(2):
        try:
            link = requests.get('https://www.finam.ru/' + fake_link)
        except:
            time.sleep(2)
            continue
        else:
            break

    if not link:
        return InterfaxNews(**{'date': '', 'header': '', 'article': '', 'link': ''})

    link.encoding = None
    b = BeautifulSoup(link.text, "html.parser")
    page = b.body.find(
            'div', attrs={'id': 'news-item'}
        )

    if not page:
        return InterfaxNews(**{'date': '', 'header': '', 'article': '', 'link': ''})

    header = find_between(str(page), start='<h1>', end='</h1>')

    if not header:
        return InterfaxNews(**{'date': '', 'header': '', 'article': ''})

    header = header.group(1)
    date_topic = find_between(
        str(page),
        start='<div class="sm lightgrey mb05 mt15">',
        end='</div>'
    ).group(1)
    date = '/'.join(reversed(str(date_topic[:10]).split('.')))
    article = page.find("div", {"class": "handmade mid f-newsitem-text"}).get_text()

    return InterfaxNews(**{
        'date': date,
        'header': header,
        'article': article,
        'link': 'https://www.finam.ru/' + fake_link
    })

def get_moex_company_page_news(
            url: str='https://www.finam.ru/profile/moex-akcii/pllc-yandex-n-v/news/',
            page: int=1
    ) -> Tuple[List[str], bool]:
    link = None
    for _ in range(2):
        try:
            link = requests.get(url + f'?page={page}')
        except:
            time.sleep(2)
            continue
        else:
            break

    if not link:
        return (None, False)

    link.encoding = None
    b = BeautifulSoup(link.text, "html.parser")

    if not b.body:
        return (None, False)

    news = b.body.find(
        'tbody', attrs={'class': 'news'}
    )
    if news:
        links = news.find_all(
            'div', {'class': 'subject'}
        )
        for_text = [
            find_between(string, start='<a href="/', end='">').
                group(1) for string in links
        ]
        return (for_text, True)
    return (None, False)

#@task
def moex_news_server_parse(
        url: Dict[str, str], date: str,  page: int,
        moex_news_dao: InterfaxNewsDAO
    ) -> pd.DataFrame:
    links, flag = get_moex_company_page_news(url=url, page=page)
    if flag:
        manager = multiprocessing.Manager()
        shared_list = manager.list()

        with Pool(15) as p:
            shared_list.append(
                p.map(
                    get_single_news,
                    links
                )
            )
        news: List[InterfaxNews] = shared_list[0]

        for single in news:
            print(single)
            print(single.date, str(pd.to_datetime(Datetime(date, only_date=True).date)))
            if single.date == str(pd.to_datetime(Datetime(date, only_date=True).date)):
                single.company = find_between(
                        list(url.keys())[0], start='moex-akcii/',
                        end='/news'
                    ).group(1)
                single.company = url[single.company]
                moex_news_dao.create(single)
    return moex_news_dao

if __name__ == "__main__":
    # com_list = dict(zip(list(tags), [match_moex_names(tag) for tag in tags]))

    import pickle

    with open('parsing/companies.json', 'rb') as fp:
        companies = pickle.load(fp)

    merge_moex_quotes_and_news(companies, save_path='~/Desktop/news_and_quotes_moex_all_years/')

    # for name in tqdm(list(companies.keys()), desc='quotes parsing'):
    #    parse_moex_quotes(
    #        ticker=companies[name],
    #        save_path=f'parsing/parse_data/moex/quotes/{companies[name]}'
    #   )

    # parse_moex_companies(
    #    list(companies.keys()),
    #    save_path='parsing/parse_data/moex/news/news.csv'
    # )