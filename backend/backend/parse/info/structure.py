from bs4 import BeautifulSoup
import requests

from typing import Dict
import base64

class TinkoffParser:
    base_url: str="https://www.tinkoff.ru/invest/stocks"

    def get_company_info(self, ticker: str) -> Dict[str, str]:
        info = {
            "ticker": ticker,
            "name": "",
            "logo": "",
            "text": "",
            "find_status": "0"
        }
        req = requests.get(f"{self.base_url}/{ticker}")
        req.encoding = "utf-8"
        soup = BeautifulSoup(req.text, "lxml")

        find = soup.find("div", class_="Column-module__column_gkBDn Column-module__column_size_12_JfttU Column-module__column_size_desktopS_8_E2WCH")

        text_body_main = find.find("div", class_="TextLineCollapse__text_IMsFF TextLineCollapse__fourOrFiveLines_z4XmE")
        if text_body_main is None:
            return info

        text_body = text_body_main.find_all("p")

        text = ""
        if len(text_body) and len(text_body) > 1:
            text = " ".join([t.text for t in text_body])
        elif len(text_body) <= 1:
            text = text_body[0].text
        info["text"] = text

        image_find = soup.find("div", class_="SecurityHeaderPure__wrapper_LD9dh")
        name = image_find.find("h2", class_="SecurityHeaderPure__titleInner_uj1Wv").find("span").text

        info["name"] = name

        image_url = "https:" + image_find.find("img", class_="InvestLogo__image_c0S9K")["src"]
        response = requests.get(image_url)
        if response.status_code == 200:
            info["logo"] = base64.b64encode(response.content).decode("utf-8")
        info["find_status"] = "1"
        return info