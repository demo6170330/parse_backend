import requests
from bs4 import BeautifulSoup

from multiprocessing import Pool
import multiprocessing

import time

from backend.other import find_between, convert_date, Datetime
from backend.storage.interfax.interfax import InterfaxNews
#from backend.backend.storage.finam.finam_impl import MongoFinamNewsDAO
#from backend.backend.tasks.task import task

from typing import List

class FinamParser():

    def __init__(self):
        pass

    @staticmethod
    def get_news_links(page: int) -> List[str]:
        url = "https://www.finam.ru/analysis/conews/"
        if page > 1:
            url += '?page={}'.format(page)
        link = requests.get(url)
        link.encoding = None
        b = BeautifulSoup(link.text, "html.parser")
        for_text = b.select('.i-user_logged_no .finam-wrap .finam-global-container .content .layout .main .inside-container')
        try:
            body = list(
                map(
                    lambda x: find_between(str(x), start='href="/analysis/newsitem/', end='"><span').group(1),
                    for_text[0].find_all('table')[1].find_all("td", {"class": "ntitle bdotline"})
                )
            )
        except:
            return None
        return body

    @staticmethod
    def get_single_news(fake_link: str) -> InterfaxNews:
        url = 'https://www.finam.ru/analysis/newsitem/' + fake_link
        link = requests.get(url)
        link.encoding = None
        b = BeautifulSoup(link.text, "html.parser")
        news = None
        try:
            for_text = b.select('.i-user_logged_no .finam-wrap .finam-global-container .content .layout .main .inside-container .f-newsitem')[0]
        except:
            news = {'date': '', 'header': '', 'article': ''}
        else:
            header = find_between(str(for_text), start='<h1>', end='</h1>').group(1)
            date_topic = find_between(
                str(for_text),
                start='<div class="sm lightgrey mb05 mt15">',
                end='</div>'
            ).group(1)
            article = for_text.find_all("div", {"class": "handmade mid f-newsitem-text"})[0].get_text()
            date = "/".join(reversed(date_topic[:10].split("."))) + " " + date_topic[11:16]
            news = {'date': date, 'header': header, 'article': article, 'link': url}
        return InterfaxNews(**news, format='', only_date=False)

    def parse_page_news(self, page: int=1, local: bool=False) -> List[InterfaxNews]:
        links: List[str] = self.get_news_links(page)

        if links is None:
            return None

        if local:
            manager = multiprocessing.Manager()
            shared_list = manager.list()
            with Pool(10) as p:
                shared_list.append(p.map(self.get_single_news, links))
            news = shared_list[0]
            return news
        return list(map(self.get_single_news, links))

if __name__ == "__main__":
    import csv
    import copy
    from multiprocessing import Manager
    from tqdm import tqdm

    p = FinamParser()
    for page in tqdm(range(1, 500), desc='parsing'):
        d_news = p.parse_page_news(page=page, local=True)
        if d_news is None:
            break

        with open('finam_news.csv', 'w') as outfile:
            w = csv.DictWriter(outfile,
                               fieldnames=['id', 'company', 'date', 'header', 'article', 'link', 'sentiment', 'tags', 'tickers'])
            w.writeheader()
            for row in d_news:
                if row.__dict__["date"] != '':
                    w.writerow(row.__dict__)

