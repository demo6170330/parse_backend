import gevent.monkey
gevent.monkey.patch_all()

import os.path
import os

import pickle
import json
import csv

import bson

from io import BytesIO

import pandas as pd

import codecs

import flask
import flask_cors

from backend.other import read_df_from_post_request

from backend.storage.interfax.interfax import InerfaxNewsNotFound, InterfaxNews
from backend.wiring import Wiring

from backend.storage.index.index_impl import Index, indices_to_pd_dataframe
from backend.storage.predictions.preds_impl import Preds, preds_to_pd_dataframe
from backend.storage.predictions.preds_dao_impl import MongoPredsDAO
from backend.storage.moex.moex_quotes import Quotes, quotes_to_pd_dataframe

from backend.utils.logger import logger_init

from backend.tasks.task import task
from backend.tasks.server_tools import check_time

from backend import dev_settings

from typing import List

from datetime import datetime

env = os.environ.get("APP_ENV", "dev")
print(f"Starting application in {env} mode")

class FinApp(flask.Flask):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        flask_cors.CORS(self)

        self.logger = logger_init(logger_name="FinApp", logger_path="", logger_level=20, file_logger_need=False,
                max_volume_of_log_file_megabytes=100)

        self.wiring = Wiring(env)

        self.sources = {
            "interfax": "news_test",
            "moex": "news_test",
            "smartlab": "news_test",
            "preds": "preds_test",
            "index": "index_test",
            "quotes": "quotes_test"
        }

        self.route("/api/v1/id/<news_id>/<source>")(self.interfax_news_by_id)
        self.route("/api/v1/date/<news_date>/<source>")(self.interfax_news_by_date)
        self.route("/api/v1/company/<company>/<source>")(self.interfax_news_by_company)
        self.route("/api/v1/latest/<company>/<source>")(self.interfax_news_by_company_last)
        self.route("/api/v1/all/<source>")(self.interfax_news_all)
        self.route("/api/v1/flags")(self.get_flags)
        self.route("/api/v1/accept_data/<source>", methods=['GET', 'POST'])(self.accept_parsed_data)

        self.route("/api/v1/info/exchange/<exchange>/<show>")(self.get_exchange_info)
        self.route("/api/v1/info/company/<company>")(self.get_company_info)
        self.route("/api/v1/info/exchanges")(self.get_exchange_list)

        self.logger.info("App initialized")

    def interfax_news_by_id(self, news_id: str, source: str="interfax"):
        self.logger.info(f"Start /api/v1/id/{news_id}/{source} method")
        if source not in list(self.sources.keys()):
            return flask.abort(404)
        try:
            news = self.wiring.mongo_client.find_many(
                collection=self.sources[source], 
                filter_exp=({"_id": bson.ObjectId(news_id)}),
                limit=500,
                is_id=False
            )
        except (InerfaxNewsNotFound, ValueError):
            return flask.abort(404)
        return json.dumps(
            [
                {
                    k: (v if not isinstance(v, datetime) else v.strftime("%Y-%m-%d %H:%M:%S"))
                    for k, v in single.items()
                    if v is not None
                }
                for single in news
            ], ensure_ascii=False
        ).encode("utf-8")

    def interfax_news_by_date(self, news_date: str, source: str="interfax"):
        self.logger.info(f"Start /api/v1/date/{news_date}/{source} method")
        if source not in list(self.sources.keys()):
            return flask.abort(404)
        try:
            news = self.wiring.mongo_client.find_many(
                collection=self.sources[source], 
                filter_exp=({"date": pd.to_datetime(news_date)}),
                limit=500,
                is_id=False
            )
        except (InerfaxNewsNotFound, ValueError):
            return flask.abort(404)
        return json.dumps(
            [
                {
                    k: (v if not isinstance(v, datetime) else v.strftime("%Y-%m-%d %H:%M:%S"))
                    for k, v in single.items()
                    if v is not None
                }
                for single in news
            ], ensure_ascii=False
        ).encode("utf-8")

    def interfax_news_all(self, source: str="interfax"):
        self.logger.info(f"Start /api/v1/all/{source} method")
        if source not in list(self.sources.keys()):
            return flask.abort(404)

        try:
            news = self.wiring.mongo_client.find_many(
                collection=self.sources[source], 
                limit=5000,
                is_id=False
            )
        except (InerfaxNewsNotFound, ValueError):
            return flask.abort(404)
        return json.dumps(
            [
                {
                    k: (v if not isinstance(v, datetime) else v.strftime("%Y-%m-%d %H:%M:%S"))
                    for k, v in single.items()
                    if v is not None
                }
                for single in news
            ], ensure_ascii=False
        ).encode("utf-8")

    def interfax_news_by_company(self, company: str=None, source: str="interfax"):
        self.logger.info(f"Start /api/v1/company/{company}/{source} method")
        if source not in list(self.sources.keys()):
            return flask.abort(404)
        try:
            news = self.wiring.mongo_client.find_many(
                collection=self.sources[source], 
                filter_exp=({"company": company}),
                limit=500,
                is_id=False
            )
        except (InerfaxNewsNotFound, ValueError):
            return flask.abort(404)
        return json.dumps(
            [
                {
                    k: (v if not isinstance(v, datetime) else v.strftime("%Y-%m-%d %H:%M:%S"))
                    for k, v in single.items()
                    if v is not None
                }
                for single in news
            ], ensure_ascii=False
        ).encode("utf-8")

    def interfax_news_by_company_last(self, company: str=None, source: str="interfax"):
        self.logger.info(f"Start /api/v1/latest/{company}/{source} method")
        if source not in list(self.sources.keys()):
            return flask.abort(404)
        try:
            news = self.wiring.mongo_client.find_many(
                collection=self.sources[source], 
                filter_exp=({"company": company}),
                is_id=False
            )
        except (InerfaxNewsNotFound, ValueError):
            return flask.abort(404)
        if not len(news):
            return json.dumps([]).encode("utf-8")
        return json.dumps(
            [
                {
                    k: (v if not isinstance(v, datetime) else v.strftime("%Y-%m-%d %H:%M:%S"))
                    for k, v in single.items()
                    if v is not None
                }
                for single in [news[-1]]
            ], ensure_ascii=False
        ).encode("utf-8")

    def get_flags(self):
        self.logger.info(f"Start /api/v1/id/flags method")
        return json.dumps(self.wiring.global_flags, ensure_ascii=False)

    def get_exchange_info(self, exchange: str="nyse", show: str=""):
        self.logger.info(f"Start /api/v1/exchange/{exchange}/{show} method")
        exchange = exchange.lower() if exchange is not None else None
        exchanges = self.wiring.exchanges
        if exchange not in exchanges:
            raise flask.abort(404)

        try:
            info = self.wiring.mongo_client.find_many(
                collection="companies", 
                filter_exp=({"exchange": exchange, "find_status": 1}) if show != "all" else ({"exchange": exchange}),
                is_id=False,
                limit=10000
            )
        except (InerfaxNewsNotFound, ValueError):
            return flask.abort(404)
        return json.dumps(
            [
                {
                    k: (v if not isinstance(v, datetime) else v.strftime("%Y-%m-%d %H:%M:%S"))
                    for k, v in single.items()
                    if v is not None
                }
                for single in info
            ], ensure_ascii=False
        ).encode("utf-8")

    def get_exchange_list(self):
        self.logger.info(f"Start /api/v1/exchanges method")
        return json.dumps(self.wiring.exchanges, ensure_ascii=False)
        
    def get_company_info(self, company: str):
        self.logger.info(f"Start /api/v1/info/company/{company}")
        info = self.wiring.company_info_crawler.get_company_info(company)
        self.wiring.logger.info(info)
        return json.dumps(
            [
                {
                    k: (v if not isinstance(v, datetime) else v.strftime("%Y-%m-%d %H:%M:%S"))
                    for k, v in single.items()
                    if v is not None
                }
                for single in [info]
            ], ensure_ascii=False
        ).encode("utf-8")

    def accept_parsed_data(self, source:str = "quotes") -> str:
        if source not in list(self.sources.keys()):
            raise ValueError('Wrong source: list of sources: [interfax, moex, finam, preds, index]')
        
        if source == "quotes":
            quotes_df = read_df_from_post_request("quotes_file")
            cat = pd.concat([self.wiring.daily_quotes_dao.collection, quotes_df], axis=0)
            cat["date"] = pd.to_datetime(cat["date"]).apply(lambda x: x.date())
            trades = cat.set_index(["company", "date"]).sort_index(level=[0, 1])
            trades = trades[~trades.index.duplicated(keep="last")].reset_index()
            self.wiring.daily_quotes_dao.mongo_database = trades
            self.wiring.daily_quotes_dao.dump_database(dev_settings.PATH_TO_QUOTES, index=False)

        elif source == "preds":
            try:
                last_preds = read_df_from_post_request("last_preds")
                last_preds = last_preds[["company","date","id","bin_prob","bin_pred","barr_prob_0","barr_prob_1","barr_prob_2","barr_pred","barr_results","bin_results"]]
            except:
                last_results = read_df_from_post_request("last_results")
                reset_preds_wiring(self.wiring, data=last_results.to_dict("records"), action="update")
            else:
                cat = pd.concat([self.wiring.preds_dao.collection, last_preds], axis=0)
                cat["date"] = pd.to_datetime(cat["date"]).apply(lambda x: x.date())
                res = cat.set_index(["company", "date"]).sort_index(level=[0, 1])
                res = res[~res.index.duplicated(keep="last")].reset_index()
                self.wiring.preds_dao.mongo_database = res
                self.wiring.preds_dao.dump_database(dev_settings.PATH_TO_PREDS, index=False)

        elif source == "smartlab":
            for d in read_df_from_post_request("smartlab_news").to_dict("records"):
                self.wiring.smartlab_news_dao.update(InterfaxNews(**d))
            self.wiring.smartlab_news_dao.dump_database(dev_settings.PATH_TO_SMARTLAB_NEWS, index=False)

        elif source == "index":
            ind = read_df_from_post_request("index")
            cat = pd.concat([self.wiring.index_dao.collection, ind], axis=0)
            cat["date"] = pd.to_datetime(cat["date"]).apply(lambda x: x.date())
            res = cat.drop_duplicates().set_index(["company", "date"]).sort_index(level=[0, 1])
            res = res[~res.index.duplicated(keep="last")].reset_index()
            self.wiring.index_dao.mongo_database = res
            self.wiring.index_dao.dump_database(dev_settings.PATH_TO_INDEX, index=False)
        return "OK"

app = FinApp('FinApp')
app.config.from_object(f"backend.{env}_settings") #backend.dev_settings.py
