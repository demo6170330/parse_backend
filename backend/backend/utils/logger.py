import logging
from logging import StreamHandler
from logging.handlers import RotatingFileHandler
from os import path
import sys


def logger_init(logger_name, logger_path, logger_level: int, file_logger_need: bool,
                max_volume_of_log_file_megabytes=100):
    # Create a log with the same name as the script that created it
    logger = logging.getLogger(logger_name)
    logger.setLevel(logger_level)
    logger.info(f'Logging "{logger_name}" initialized.')

    streamformatter = logging.Formatter(fmt='[%(asctime)s]:%(levelname)s:%(threadName)s:%(levelname)s:%(name)s:\t%(message)s',
                                        datefmt='%Y-%m-%d %H:%M:%S')

    # instancing loggers for std and set them filters for message types
    info_streamhandler = logging.StreamHandler(stream=sys.stdout)
    info_streamhandler.addFilter(lambda record: record.levelno <= logging.INFO)
    info_streamhandler.setFormatter(streamformatter)

    err_streamhandler = logging.StreamHandler(stream=sys.stderr)
    err_streamhandler.addFilter(lambda record: record.levelno >= logging.WARNING)
    err_streamhandler.setFormatter(streamformatter)

    if file_logger_need:
        log_file_path = path.join(logger_path, f'{logger_name}.log')
        filehandler_dbg = RotatingFileHandler(log_file_path,
                                              maxBytes=max_volume_of_log_file_megabytes * 1024 * 1024,
                                              backupCount=2)
        filehandler_dbg.setLevel(logger_level)
        filehandler_dbg.setFormatter(streamformatter)
        logger.addHandler(filehandler_dbg)

    # Add handlers to logger
    logger.addHandler(info_streamhandler)
    logger.addHandler(err_streamhandler)

    logger.info('New session started')

    return logger