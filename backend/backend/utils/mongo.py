from typing import List, Union, Iterable
from urllib.parse import urlparse

import gridfs
import pymongo
from bson import ObjectId  # noqa

import pandas as pd

from backend.utils.logger import logger_init


class MongoDBAdapter:
    _connection = None
    db = None
    col = None
    grid_fs = None

    def __init__(self, uri: str):
        self._uri = uri
        database_name = urlparse(uri).path[1:]
        self.db_name = database_name

        self.logger = logger_init(logger_name="MongoDBAdapter", logger_path="", logger_level=20, file_logger_need=False,
                max_volume_of_log_file_megabytes=100)

    def connect(self):
        motor_client = pymongo.MongoClient(self._uri)

        self._connection = motor_client
        self.db = motor_client[self.db_name]
        self.grid_fs = gridfs.GridFS(self.db)
        return self

    def find_one(self,
                 collection: str,
                 filter_exp: dict = None,
                 sort: List[tuple] = None,
                 is_id: bool = True,
                 ) -> dict:
        filter_exp = filter_exp or {}
        if isinstance(filter_exp, tuple):
            find_query = filter_exp[0]
            projection = filter_exp[-1]
        else:
            find_query = filter_exp
            projection = None
        cursor = self.db[collection].find(find_query, projection=projection)
        limit = 1
        if sort:
            cursor.sort(sort).limit(limit)
        else:
            cursor.limit(limit)
        for document in cursor:
            if not is_id and document:
                document.pop('_id')
            return document
        return {}

    def _find_many_cursor(self,
                          collection: str,
                          filter_exp: Union[dict, tuple],
                          sort: List[tuple] = None,
                          skip: int = 0,
                          limit: int = 1000):
        if isinstance(filter_exp, tuple):
            find_query = filter_exp[0]
            projection = filter_exp[-1]
        else:
            find_query = filter_exp
            projection = None

        cursor = self.db[collection].find(find_query, projection=projection)
        if sort:
            cursor.sort(sort).skip(skip)#.limit(limit)
        else:
            cursor.skip(skip)#.limit(limit)
        return cursor

    def find_many(self,
                  collection: str,
                  filter_exp: Union[dict, tuple] = None,
                  sort: List[tuple] = None,
                  skip: int = 0,
                  limit: int = 5000,
                  is_id: bool = True) -> List[dict]:
        filter_exp = filter_exp or {}
        cursor = self._find_many_cursor(collection, filter_exp, sort, skip, limit)

        documents = []

        for document in list(cursor)[-limit:]: #.to_list(length=limit):
            if is_id is False and document:
                document.pop('_id')
            documents.append(document)
        return documents or []

    def find_iter(self,
                  collection: str,
                  filter_exp: Union[dict, tuple],
                  sort: List[tuple] = None,
                  skip: int = 0,
                  limit: int = 1000,
                  is_id: bool = True) -> Iterable[dict]:
        cursor = self._find_many_cursor(collection, filter_exp, sort, skip, limit)
        for document in cursor.to_list(length=limit):
            if is_id is False and document:
                document.pop('_id')
            yield document

    def count(self,
              collection: str,
              filter_exp: dict = None) -> int:
        filter_exp = filter_exp or {}
        return self.db[collection].count_documents(filter_exp)
        
    def get_max(self, collection, col:str, filter_exp: Union[dict, tuple]=({})):
        find = self._find_many_cursor(collection=collection, filter_exp=filter_exp)
        cursor = find.sort(col, -1).limit(1)
        docs = list(cursor)
        return docs

    def insert_one(self, collection: str, document: dict) -> dict:
        return self.db[collection].insert_one(document)

    def update_one(self, collection: str, filter_exp: dict, update_clause: dict, upsert: bool = False) -> dict:
        return self.db[collection].update_one(filter_exp, update_clause, upsert=upsert)

    def update_many(self, collection: str, filter_exp: Union[dict, tuple], update_clause: dict, upsert: bool = False) -> dict:
        return self.db[collection].update_many(filter_exp, update_clause, upsert=upsert)

    def replace_one(self, collection: str, filter_exp: dict, update_clause: dict, upsert: bool = False):
        return self.db[collection].replace_one(filter_exp, update_clause, upsert=upsert)

    def delete_one(self, collection: str, filter_exp: dict) -> dict:
        return self.db[collection].delete_one(filter_exp)

    def delete_many(self, collection: str, filter_exp: dict) -> dict:
        return self.db[collection].delete_many(filter_exp)

    def create_simple_index(self, collection, field, background=True):
        with pymongo.MongoClient(self._uri) as sync_client:
            sync_client[self.db_name][collection].create_index([(field, 'text')], background=background)

    def insert_file(self, file_name: str, file_bytes: bytes) -> str:
        fileid = self.grid_fs.put(file_bytes, filename=file_name)
        return fileid

    def get_file(self, file_id: str) -> bytes:
        fileid_obj = ObjectId(file_id)
        grid_out = self.grid_fs.get(fileid_obj).read()
        return grid_out

    def delete_file(self, file_id: str):
        fileid_obj = ObjectId(file_id)
        self.grid_fs.delete(fileid_obj)

    def from_frame(self, frame: pd.DataFrame, collection: str):
        records = frame.to_dict("records")
        for rec in records:
            self.insert_one(collection, rec)

    def from_csv(self, csv: str, collection):
        frame = pd.read_csv(csv).fillna("-1")
        #frame["date"] = pd.to_datetime(frame["date"])
        #frame["exchange"] = ["nyse" for i in range(len(frame))]
        self.from_frame(frame, collection)

    def find_min_max_value_in_company(self, collection: str, filter_exp: Union[dict, tuple]=({})) -> pd.Timestamp:
        collection = self.db[collection]
        agg_result = collection.aggregate(
            [
                {"$match": filter_exp},
                
                {
            "$group" :
                {"_id" : "$company",
                "max": {'$max': "$date"}
                    }
                }
            ])
        frame = pd.DataFrame(agg_result)
        if "max" in frame.columns.tolist():
            return frame["max"].min()
        return None
        

