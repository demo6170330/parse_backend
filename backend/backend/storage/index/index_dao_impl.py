from typing import Iterable

import bson
import bson.errors

from backend.storage.index.index_impl import Index, IndicesNotFoundError
from backend.other import Datetime

import pandas as pd

from typing import List

class MongoIndexDAO(Index):

    def __init__(self, mongo_database: pd.DataFrame):
        self.mongo_database = mongo_database

    @property
    def collection(self) -> pd.DataFrame:
        return self.mongo_database

    def load_database(self, path: str, indices: List[str]=None):
        data = pd.read_csv(path)
        if indices:
            data = data.set_index(indices)
        self.mongo_database = data

    def dump_database(self, path: str, index: bool=False):
        self.collection.to_csv(path, index=index)

    @classmethod
    def to_bson(cls, news: Index):
        result = {
            k: v
            for k, v in news.__dict__.items()
            if v is not None
        }
        #if "id" in result:
        #    result["_id"] = bson.ObjectId(result.pop("id"))
        return result

    @classmethod
    def from_bson(cls, document) -> Index:
        #document["id"] = str(document.pop("_id"))
        return Index(**document)

    def create(self, news: Index) -> Index:
        if news.date != '' and news.date is not None:
            #news.id = str(
            #    self.collection.insert_one(
            #        self.to_bson(news)
            #    ).inserted_id
            #)
            self.mongo_database = self.mongo_database.append(news.__dict__, ignore_index=True)
            return news

    def update(self, news: Index) -> Index:
        mask = self.collection["id"] == news.id
        self.mongo_database.loc[mask, self.collection.columns] = list(news.__dict__.values())

    def get_all(self) -> Iterable[Index]:
        for doc in self.collection.to_dict('records'):
            yield self.from_bson(doc)

    def get_by_id(self, news_id: str) -> Index:
        sample = self._get_by_query({"id": news_id})
        return sample

    def get_by_date(self, date: str):
        docs = self._get_by_date(Datetime(date, only_date=True))
        return docs

    def get_by_company(self, company: str):
        docs = self._get_by_company(company)
        return docs

    def _get_by_company(self, company: str):
        docs = self.collection[self.collection["company"] == company]
        if not len(docs):
            raise IndicesNotFoundError()
        for doc in docs.to_dict('records'):
            yield self.from_bson(doc)

    def _get_by_date(self, date: Datetime) -> Iterable[Index]:
        col = self.collection.copy()
        col["date"] = pd.to_datetime(col["date"].apply(lambda x: str(x[:10])))
        
        docs = col[col["date"] == pd.to_datetime(date.date, format="%Y-%m-%d")]
        if not len(docs):
            raise IndicesNotFoundError()
        for doc in docs.to_dict('records'):
            yield self.from_bson(doc)

    def _get_by_query(self, query) -> Index:
        key, value = query.items()
        doc = self.collection[self.collection[key[0] == value[0]]]
        if not len(doc):
            raise IndicesNotFoundError()
        return self.from_bson(doc.to_dict('records')[0])