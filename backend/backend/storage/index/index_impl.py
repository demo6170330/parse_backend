from typing import List
import pandas as pd
import bson

from backend.other import Datetime

class Index(object):

    def __init__(
        self, id: str=None, company: str=None,
        date: str=None, value: str=None
    ):

        self.id = id if id else str(bson.objectid.ObjectId())
        self.company = company if company else ''
        self.date = Datetime(date).date if date else ''
        self.value = value

    def __repr__(self):
        return f'Index(' \
                    f'id={self.id}, ' \
                    f'company={self.company}, ' \
                    f'date={self.date}, ' \
                    f'value={self.value}' \
               f')'

def indices_to_pd_dataframe(indices: List[Index], cast_cols: bool=False) -> pd.DataFrame:
    indices_df = pd.DataFrame([
        ind.__dict__ for ind in indices
    ])
    if cast_cols:
        indices_df["date"] = pd.to_datetime(indices_df["date"])
        for c in ("value"):
            indices_df[c] = indices_df[c].astype(float)
    return indices_df

class IndicesNotFoundError(Exception):
    pass
