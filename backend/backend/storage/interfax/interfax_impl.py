import bson
import bson.errors

from backend.storage.interfax.interfax import InterfaxNews, InerfaxNewsNotFound
from backend.storage.moex.moex_quotes import Quotes
from backend.other import Datetime

from backend.utils.mongo import MongoDBAdapter

import pandas as pd

from typing import List, Union, Iterable


class MongoWrapper:
    
    def __init__(self, client: MongoDBAdapter,
                  collection: str, 
                  filter_exp: Union[dict, tuple] = None,
                  sort: List[tuple] = None,
                  skip: int = 0,
                  limit: int = 5000,
                  is_id: bool = True):
        self.client = client
        self.collection = collection
        self.filter_exp = filter_exp
        self.sort = sort
        self.skip = skip
        self.limit = limit
        self.is_id = is_id

    @property
    def collection(self) -> pd.DataFrame:
        docs = self.client.find_many(self.collection, self.filter_exp, self.sort, self.skip, self.limit, self.is_id)
        return pd.DataFrame(docs)

class MongoInterfaxNewsDAO(InterfaxNews):

    def __init__(self, mongo_database: pd.DataFrame):
        self.mongo_database = mongo_database

    def set_wrapper(wrapper: MongoWrapper):
        self.mongo_database = wrapper.collection.copy()

    @property
    def collection(self) -> pd.DataFrame:
        return self.mongo_database

    def load_database(self, path: str, indices: List[str]=None):
        data = pd.read_csv(path)
        if indices:
            data = data.set_index(indices)
        self.mongo_database = data

    def dump_database(self, path: str, index: bool=False):
        self.collection.to_csv(path, index=index)

    @classmethod
    def to_bson(cls, news: InterfaxNews):
        result = {
            k: v
            for k, v in news.__dict__.items()
            if v is not None
        }
        #if "id" in result:
        #    result["_id"] = bson.ObjectId(result.pop("id"))
        return result

    @classmethod
    def from_bson(cls, document) -> InterfaxNews:
        #document["id"] = str(document.pop("_id"))
        return InterfaxNews(**document)

    def create(self, news: InterfaxNews) -> InterfaxNews:
        if news.date != '' and news.date is not None:
            #news.id = str(
            #    self.collection.insert_one(
            #        self.to_bson(news)
            #    ).inserted_id
            #)
            self.mongo_database = self.mongo_database.append(news.__dict__, ignore_index=True)
            return news

    def update(self, news: InterfaxNews) -> InterfaxNews:
        mask = self.collection["id"] == news.id
        self.mongo_database.loc[mask, self.collection.columns] = list(news.__dict__.values())

    def get_all(self) -> Iterable[InterfaxNews]:
        for doc in self.collection.to_dict('records'):
            yield self.from_bson(doc)

    def get_by_id(self, news_id: str) -> InterfaxNews:
        sample = self._get_by_query({"id": news_id})
        return sample

    def get_by_date(self, date: str):
        docs = self._get_by_date(Datetime(date, only_date=True))
        return docs

    def get_by_company(self, company: str):
        docs = self._get_by_company(company)
        return docs

    def get_by_company_last(self, company: str) -> List[InterfaxNews]:
        *_, last = self.get_by_company(company)
        return [last]

    def _get_by_company(self, company: str):
        docs = self.collection[self.collection["company"] == company]
        if not len(docs):
            raise InerfaxNewsNotFound
        for doc in docs.to_dict('records'):
            yield self.from_bson(doc)

    def _get_by_date(self, date: Datetime) -> Iterable[InterfaxNews]:
        col = self.collection.copy()
        col["date"] = pd.to_datetime(col["date"].apply(lambda x: str(x[:10])))
        
        docs = col[col["date"] == pd.to_datetime(date.date, format="%Y-%m-%d")]
        if not len(docs):
            raise InerfaxNewsNotFound
        for doc in docs.to_dict('records'):
            yield self.from_bson(doc)

    def _get_by_query(self, query) -> InterfaxNews:
        key, value = query.items()
        doc = self.collection[self.collection[key[0] == value[0]]]
        if not len(doc):
            raise InerfaxNewsNotFound()
        return self.from_bson(doc.to_dict('records')[0])

class MongoQuotesDAO(Quotes):

    def __init__(self, mongo_database: pd.DataFrame):
        self.mongo_database = mongo_database

    @property
    def collection(self) -> pd.DataFrame:
        return self.mongo_database

    def load_database(self, path: str, indices: List[str]=None):
        data = pd.read_csv(path)
        if indices:
            data = data.set_index(indices)
        self.mongo_database = data

    def dump_database(self, path: str, index: bool=False):
        self.collection.to_csv(path, index=index)

    @classmethod
    def to_bson(cls, news: Quotes):
        result = {
            k: v
            for k, v in news.__dict__.items()
            if v is not None
        }
        #if "id" in result:
        #    result["_id"] = bson.ObjectId(result.pop("id"))
        return result

    @classmethod
    def from_bson(cls, document) -> Quotes:
        #document["id"] = str(document.pop("_id"))
        return Quotes(**document)

    def create(self, news: Quotes) -> Quotes:
        if news.date != '' and news.date is not None:
            #news.id = str(
            #    self.collection.insert_one(
            #        self.to_bson(news)
            #    ).inserted_id
            #)
            self.mongo_database = self.mongo_database.append(news.__dict__, ignore_index=True)
            return news

    def update(self, news: Quotes) -> Quotes:
        mask = self.collection["id"] == news.id
        self.mongo_database.loc[mask, self.collection.columns] = list(news.__dict__.values())

    def get_all(self) -> Iterable[Quotes]:
        for doc in self.collection.to_dict('records'):
            yield self.from_bson(doc)

    def get_by_id(self, news_id: str) -> Quotes:
        sample = self._get_by_query({"id": news_id})
        return sample

    def get_by_date(self, date: str):
        docs = self._get_by_date(Datetime(date, only_date=True))
        return docs

    def get_by_company(self, company: str):
        docs = self._get_by_company(company)
        return docs

    def get_by_company_last(self, company: str) -> List[Quotes]:
        *_, last = self.get_by_company(company)
        return [last]

    def _get_by_company(self, company: str):
        docs = self.collection[self.collection["company"] == company]
        if not len(docs):
            raise InerfaxNewsNotFound
        for doc in docs.to_dict('records'):
            yield self.from_bson(doc)

    def _get_by_date(self, date: Datetime) -> Iterable[Quotes]:
        col = self.collection.copy()
        col["date"] = pd.to_datetime(col["date"].apply(lambda x: str(x[:10])))
        
        docs = col[col["date"] == pd.to_datetime(date.date, format="%Y-%m-%d")]
        if not len(docs):
            raise InerfaxNewsNotFound
        for doc in docs.to_dict('records'):
            yield self.from_bson(doc)

    def _get_by_query(self, query) -> Quotes:
        key, value = query.items()
        doc = self.collection[self.collection[key[0] == value[0]]]
        if not len(doc):
            raise InerfaxNewsNotFound()
        return self.from_bson(doc.to_dict('records')[0])