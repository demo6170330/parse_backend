# flask_app/backend/storage/interfax.py

import abc
from typing import Iterable, Dict
import pandas as pd
import bson

from backend.other import Datetime

class InterfaxNews(object):

    def __init__(self, id: str=None, company: str=None, date: str=None,
                 header: str=None, article: str=None, link: str=None,
                 sentiment: str=None, tags: str=None, tickers: str=None, format: str='%Y/%m/%d',
                 only_date: bool=True, flag_convert: bool=False):
        self.id = id if id else str(bson.objectid.ObjectId())
        self.company = company if company else ''
        self.date = Datetime(date, format=format, only_date=only_date, flag_convert=flag_convert).date if date else ''
        self.header = header
        self.article = article
        self.link = link
        self.sentiment = sentiment
        self.tags = tags
        self.tickers = tickers

    def __repr__(self):
        return f'InterfaxNews(' \
                    f'id={self.id}, ' \
                    f'company={self.company}, ' \
                    f'date={self.date}, ' \
                    f'header={self.header} ,' \
                    f'link={self.link})' \
               f')'

class InterfaxNewsDAO(object):

    @abc.abstractmethod
    def create(self, news: InterfaxNews) -> InterfaxNews:
        pass

    @abc.abstractmethod
    def update(self, news: InterfaxNews) -> InterfaxNews:
        pass

    @abc.abstractmethod
    def get_all(self) -> Iterable[InterfaxNews]:
        pass

    @abc.abstractmethod
    def get_by_id(self, news_id: str) -> InterfaxNews:
        pass

    @abc.abstractmethod
    def get_by_date(self, date: str) -> Iterable[InterfaxNews]:
        pass

class InerfaxNewsNotFound(Exception):
    pass

if __name__ == "__main__":
    print(type(bson.objectid.ObjectId()))