from typing import List
import pandas as pd
import bson

from backend.other import Datetime

class Quotes(object):

    def __init__(
        self, id: str=None, company: str=None,
        date: str=None, per: str=None,
        time: str=None, open: str=None,
        high: str=None, low: str=None,
        close: str=None, vol: str=None,
        average_open_hourly_price: str=None
    ):
        #['company','<PER>','date','<TIME>','<OPEN>','<HIGH>','<LOW>','<CLOSE>','<VOL>']
        self.id = id if id else str(bson.objectid.ObjectId())
        self.company = company if company else ''
        self.date = Datetime(date).date if date else ''
        self.per = per
        self.time = time
        self.open = open
        self.high = high
        self.low = low
        self.close = close
        self.vol = vol
        self.average_open_hourly_price = average_open_hourly_price

    def __repr__(self):
        return f'Quotes(' \
                    f'id={self.id}, ' \
                    f'company={self.company}, ' \
                    f'date={self.date}, ' \
                    f'high={self.high} ,' \
                    f'vol={self.vol})'\
                    f'close={self.close}'\
               f')'

def quotes_to_pd_dataframe(quotes: List[Quotes], cast_cols: bool=False) -> pd.DataFrame:
    quotes_df = pd.DataFrame([
        quote.__dict__ for quote in quotes
    ])
    if cast_cols:
        quotes_df["date"] = pd.to_datetime(quotes_df["date"])
        for c in ("open", "high", "low", "close", "vol"):
            quotes_df[c] = quotes_df[c].astype(float)
    return quotes_df

class QuotesNotFoundError(Exception):
    pass
