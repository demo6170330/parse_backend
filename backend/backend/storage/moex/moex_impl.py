from backend.storage.interfax.interfax_impl import MongoInterfaxNewsDAO

import pandas as pd

class MongoMOEXNewsDAO(MongoInterfaxNewsDAO):

    def __init__(self, mongo_database: pd.DataFrame):
        self.mongo_database = mongo_database

    @property
    def collection(self) -> pd.DataFrame:
        return self.mongo_database