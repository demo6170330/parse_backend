from typing import List
import pandas as pd
import bson

from backend.other import Datetime

class Preds(object):

    def __init__(
        self, id: str=None, company: str=None,
        date: str=None, bin_prob: str=None, bin_pred: str=None,
        barr_prob_0: str=None, barr_prob_1: str=None,
        barr_prob_2: str=None, barr_pred: str=None,
        barr_results: str=None, bin_results: str=None
    ):

        self.id = id if id else str(bson.objectid.ObjectId())
        self.company = company if company else ''
        self.date = Datetime(date).date if date else ''
        self.bin_prob = bin_prob
        self.bin_pred = bin_pred
        self.barr_prob_0 = barr_prob_0
        self.barr_prob_1 = barr_prob_1
        self.barr_prob_2 = barr_prob_2
        self.barr_pred = barr_pred
        self.barr_results = barr_results
        self.bin_results = bin_results

    def __repr__(self):
        return f'Preds(' \
                    f'id={self.id}, ' \
                    f'company={self.company}, ' \
                    f'date={self.date}, ' \
                    f'bin results={self.bin_results}, ' \
                    f'bin pred={self.bin_pred}, ' \
                    f'barr results={self.barr_results}, ' \
                    f'barr pred={self.barr_pred}, ' \
               f')'

def preds_to_pd_dataframe(preds: List[Preds], cast_cols: bool=False) -> pd.DataFrame:
    preds_df = pd.DataFrame([
        pred.__dict__ for pred in preds
    ])
    if cast_cols:
        preds_df["date"] = pd.to_datetime(preds_df["date"])
        for c in ("bin_prob", "bin_pred", "barr_prob_0", "barr_prob_1", "barr_prob_2", "barr_pred", "barr_results", "bin_results"):
            preds_df[c] = preds_df[c].astype(float)
    return preds_df

class PredsNotFoundError(Exception):
    pass
