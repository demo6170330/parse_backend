# flask_app/backend/wiring.py

import os

import redis
import rq

import backend.dev_settings

from backend.parse.moex.get_quotes import ExchangeCrawler
from backend.parse.info.structure import TinkoffParser
from backend.parse.smartlab.base.smartlab_parse_impl import SmartLabNewsParser

from backend.utils.mongo import MongoDBAdapter
from backend.utils.logger import logger_init

import pandas as pd
import json

import pickle

class Wiring(object):

    def __init__(self, env=None):
        if env is None:
            env = os.environ.get("APP_ENV", "dev")
        self.settings = {
            "dev": backend.dev_settings,
            # (добавьте сюда настройки других
            # окружений, когда они появятся!)
        }[env]

        # С ростом числа компонент этот код будет усложняться.
        # В будущем вы можете сделать тут такой DI, какой захотите.
        #self.mongo_client: MongoClient = MongoClient(
        #    host=self.settings.MONGO_HOST,
        #    port=self.settings.MONGO_PORT
        #)
        
        #self.interfax_news_dao: InterfaxNewsDAO = MongoInterfaxNewsDAO(self.mongo_database)
        #self.finam_news_dao: InterfaxNewsDAO = MongoFinamNewsDAO(self.mongo_database)

        self.mongo_client = MongoDBAdapter(os.environ.get("MONGO_URI", "mongodb://45.10.40.100:27017/test")).connect()

        self.logger = logger_init(logger_name="Wiring", logger_path="", logger_level=20, file_logger_need=False,
                max_volume_of_log_file_megabytes=100)

        self.logger.info(os.environ.get("REDIS_HOST"))
        self.exchange_crawler: ExchangeCrawler = ExchangeCrawler()
        self.company_info_crawler: TinkoffParser = TinkoffParser()
        self.smartlab_news_crawler: SmartLabNewsParser = SmartLabNewsParser()

        self.global_flags = {
            "moex": False,
            "smartlab": False,
            "quotes": False,
            "quotes_usa": False
        }
        self.exchanges = ("nyse", "moex", "binance")
        
        self.parsing_start_hour = int(os.environ.get("PARSING_START_HOUR", "22"))
        self.reset_global_flags_hour = int(os.environ.get("RESET_GLOBAL_FLAGS_HOUR", "21"))

        self.ml_url = os.environ.get("ML_URL", "http://45.10.40.100:3389/parse")

        self.redis: redis.Redis = redis.StrictRedis(
            host = os.environ.get("REDIS_HOST"),
            port = int(os.environ.get("REDIS_PORT", "6379")),
            db = int(os.environ.get("REDIS_DB", "0"))
        )
        self.TASK_QUEUE_NAME = os.environ.get("REDIS_TASK_QUEUE_NAME", "tasks")
        #self.settings["TASK_QUEUE_NAME"] = os.environ.get("REDIS_TASK_QUEUE_NAME", "tasks")
        self.task_queue: rq.Queue = rq.Queue(
            name = self.TASK_QUEUE_NAME,
            connection = self.redis
        )
