import re
import pandas as pd

from io import BytesIO
from flask import request

def find_between(base_string: str, start: str='data-id=', end: str='>'):
    return re.search('%s(.*)%s' % (start, end), str(base_string))

def convert_date(date: str):
    new_date = date[-2:]
    if int(new_date) > 99:
        return f'{date[:-3]}.19{new_date} '
    return f'{date[:-3]}.20{new_date} '

def reverse_date(date: str) -> str: # input example 02.10.2020
    return '.'.join(reversed(date.split('.')))

def timestamp_to_str(date: pd.Timestamp) -> str:
    """
    Timestamp to string converter
    :param date: pd.Timestamp
        format example: pd.to_datetime('2021-01-01 01:00:00')
    :return:
        string date:
            format example: '2021-01-01'
    """
    return str(date)[:10]

class Datetime(object):

    def __init__(self, date: str, flag_convert: bool=False, only_date: bool=True, format: str="%Y/%m/%d"): #input example: 2020.10.02 - yyyy.mm.dd
        self.date = self.convert_to_pandas_datetime(date, flag_convert=flag_convert, only_date=only_date, format=format)

    @staticmethod
    def convert_to_pandas_datetime(date: str, flag_convert: bool=False, only_date: bool=True, format: str='%Y/%m/%d') -> str:
        d = date
        if flag_convert:
            d = reverse_date(convert_date(date))
            d = '/'.join(d.split('.'))
            d = ''.join(d.split(" "))
        d = pd.to_datetime(d, format=format)
        if only_date:
            return str(d.date())    #output example: 2020-10-02
        return str(d)   #output example: 2020-10-02 23:59:00

def read_df_from_post_request(post_filename: str) -> pd.DataFrame:
    content = request.files[post_filename]
    bio = BytesIO()
    bio.write(content.read())
    bio.seek(0)
    df = pd.read_csv(bio)
    return df