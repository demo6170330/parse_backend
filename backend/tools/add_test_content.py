# flask_app/tools/add_test_content.py

from backend.storage.interfax import InterfaxNews
from backend.wiring import Wiring

wiring = Wiring()

wiring.interfax_news_dao.create(
    InterfaxNews(
        date='2020.05.10',
        header='прошый день то2twetргов',
        article='Лукойла2 рtfeeqезко упали за прошый день торгов'
    )
)

wiring.interfax_news_dao.create(
    InterfaxNews(
        date='2020.05.10',
        header='прошыrqwйdq Газпром день торгов',
        article='рошdwqый д2ень торгов'
    )
)

wiring.interfax_news_dao.create(
    InterfaxNews(
        date='2020.05.10',
        header='прошыrqwйdq Газпром fqдень торгов',
        article='рошdwqый д2ень торгоfqв'
    )
)

wiring.interfax_news_dao.create(
    InterfaxNews(
        date='2020.06.10',
        header='прошыfqrqwйdq Газпром день торгов',
        article='рошdwqefый д2ень торгов'
    )
)

wiring.interfax_news_dao.create(
    InterfaxNews(
        date='2020.05.10',
        header='проfqшыrqwйdq Газпром день торгов',
        article='рошdfqwqый д2ень торгов'
    )
)