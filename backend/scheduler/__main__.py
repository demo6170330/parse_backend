from backend.wiring import Wiring
from backend.tasks.task import task
from backend.tasks.server_tools import check_time

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler

import atexit
import os

def main():
    env = os.environ.get("APP_ENV", "dev")
    wiring = Wiring(env)
    scheduler = BlockingScheduler(job_defaults={"coalesce": False})
    scheduler.add_job(
            check_time, trigger='interval', seconds=int(os.environ.get("SECONDS_PER_CHECK", "3600")),
            kwargs={"wiring": wiring}
        )
    scheduler.start()
    atexit.register(lambda: scheduler.shutdown())

if __name__ == "__main__":
    main()