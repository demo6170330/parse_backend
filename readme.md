## Парсер для трейдингового проекта
Собирает новости со Smartlab, MOEX, а также котировки.


## Как собрать и запустить
```console
foo@bar:~$ cd ~/path
foo@bar:~/path$ git clone https://git.miem.hse.ru/395/parse_backend.git
foo@bar:~/path$ cd parse_backend
foo@bar:~/path/parse_backend$ sudo docker-compose build
foo@bar:~/path/parse_backend$ sudo docker-compose up -d
```
## Методы API
список источников (domain_names): [smartlab, moex, quotes, preds, index]
company_name - тикер компании
- Получение данных по новостям или котировкам по тикеру:
    - http://172.16.1.202:5000/api/v1/company/{company_name}/{domain_name}     (без {})
    - Если используете smartlab, moex, то получите новости. Если quotes - котировки, если preds - предсказания, index - индекс
    - Пример - http://172.16.1.202:5000/api/v1/company/SBER/quotes (котировки Сбербанка с 2015 года)
- Получение данных по новостям или котировкам по дате
    - http://172.16.1.202:5000/api/v1/date/{date}/{domain_name}     (без {})
    - Пример - http://172.16.1.202:5000/api/v1/date/2021-03-26/smartlab
- В случае не верно указанной даты, названия или источника кидается Internal Server Error

## Описание возвращаемых json-ов
* preds
---
|  id | company  | date | bin_prob | bin_pred | barr_prob_0 | barr_prob_1 | barr_prob_2 | barr_pred | barr_results | bin_results |
|:------------- |:---------------:| -------------:| -------------:| -------------:| -------------:| -------------:| -------------:| -------------:| -------------:| -------------:|
| "607c817899e55158214c57a2"      | "RASP" |     "2021-04-19" | 0.8989293758292201 | 1 | 0.0390598381394907 | 0.06547995967833221 | 0.06547995967833221 | 0.895460202182177 | -3 | -3 |
| bson hashed id | имя тикера       |     дата   | вероятность повышения цены (бинарная модель) | предсказание бинарной модели | вероятность понижения цены (барьерная модель) | вероятность колебания цены в барьере (барьерная модель) | вероятность повышения цены (барьерная модель) | предсказание модели (барьерная модель) | результат (барьерная модель) | результат (бинарная модель)

Если делать запрос на дату следующего не наступившего торгового дня, то в results вернётся -3, а если на ранние дни, то там будет результат торгового дня

---
* index

|  id | company  | date | value |
|:------------- |:---------------:| -------------:| -------------:|
| "607c817899e55158214c57a2"      | "RASP" |     "2021-04-16" | 20.35299606676781 |
| bson hashed id | имя тикера       |     дата   | значение |
---
Индекс является поддерживающим, т.е. в случае, когда у нас есть предсказания на следующий торговый день, основанные на текущем дне, мы не можем получить значение индекса на следующий день, как с предсказаниями

* quotes
---

|  id | company | date | open | high | low | close | vol | average_open_hourly_price |
|:------------- |:---------------:| -------------:| -------------:| -------------:| -------------:| -------------:| -------------:| -------------:|
| "607d670a772e8ed480b6e585"     | "SBER" |     "2021-04-16" | 285.7 | 289.3 | 284.08 | 288.5 | 63355410.0 | 287.46714285714285 |
| bson hashed id | имя тикера       |     дата   | цена открытия дня | максимум дня | минимум дня | цена закрытия дня | объём торгов дня | значение усреднённых часовых цен открытия за день|

---
* smartlab, moex
---
|  id | company | date | header | article | link | sentiment | tags | tickers|
|:------------- |:---------------:| -------------:| -------------:| -------------:| -------------:| -------------:| -------------:| -------------:|
| bson hashed id | имя тикера       |     дата   | заголовок | текст новости | ссылка на сайт с новостью | тональность |  тематические теги для новости | теги тикеров для новости |
---